package www.larkmidtable.com.writer;

import www.larkmidtable.com.bean.ConfigBean;
import www.larkmidtable.com.constant.WriterPluginEnum;
import www.larkmidtable.com.element.Record;
import www.larkmidtable.com.exception.YunQueException;
import www.larkmidtable.com.log.LogRecord;
import www.larkmidtable.com.model.TaskParams;
import www.larkmidtable.com.model.TaskResult;

import java.io.IOException;
import java.util.Queue;
import java.util.function.Supplier;

/**
 *
 *
 * @Date: 2022/11/14 11:03
 * @Description:
 **/
public abstract class Writer {

	protected Queue<Record> queue;

	protected ConfigBean configBean;

	protected LogRecord logRecord;

	public ConfigBean getConfigBean() {
		return configBean;
	}

	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}

	public static Writer getWriterPlugin(String name, ConfigBean writerConfigBean) {
		try {
			Writer writer = (Writer) Class.forName(WriterPluginEnum.getByName(name).getClassPath()).newInstance();
			writer.setConfigBean(writerConfigBean);
			return writer;
		} catch (Exception e) {
			throw new YunQueException("文件获取不到", e);
		}
	}

	// 初始化操作
	public abstract void init();

	public abstract void write(Queue<Record> queue);

	// 初始化操作
	@Deprecated
	public void open() {};

	// 开始写操作
	@Deprecated
	public void startWrite() throws InterruptedException {};

	// 关闭操作
	@Deprecated
	public void close() throws IOException {};

	// 关闭操作
	public abstract void stop();

	public abstract class WriteTask<P extends TaskParams, R extends TaskResult> implements Supplier<TaskResult> {
		protected P taskParams;

		public WriteTask(P taskParams) {
			this.taskParams = taskParams;
		}

		public abstract void preProcess();
		public abstract R doProcess();
		public abstract R postProcess(R processResult);
		@Override
		public R get() {
			preProcess();
			return postProcess(doProcess());
		}
	}
}
