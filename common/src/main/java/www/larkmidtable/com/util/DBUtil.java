package www.larkmidtable.com.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import www.larkmidtable.com.bean.ConfigBean;
import www.larkmidtable.com.exception.YunQueException;

import java.sql.*;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static www.larkmidtable.com.exception.CommonErrorCode.CONFIG_ERROR;
import static www.larkmidtable.com.util.ExitCode.PARAMERRORKEXIT;

/**
 * @author stave_zhao
 * @title: DBUtil
 * @projectName yunque
 * @description: 数据库连接工具类
 * @date 2022/11/1509:13
 */
public class DBUtil {

	private static final Logger LOG = LoggerFactory.getLogger(DBUtil.class);

	/**
	 * 获取数据库连接
	 *
	 * @param databaseDriver 驱动类型（根据数据库类型选择）
	 * @param jdbcUrl        数据库连接信息
	 * @param username       用户名
	 * @param password       密码
	 * @return
	 */
	public static Connection getConnection(String databaseDriver, String jdbcUrl, String username, String password)
			throws SQLException {
		// 连接数据库
		return DriverManager.getConnection(jdbcUrl, username, password);
	}

	/**
	 * 关闭连接
	 *
	 * @param stmt
	 * @param conn
	 */
	public static void close(PreparedStatement stmt) {
		close(stmt, null, null);
	}

	/**
	 * 关闭连接
	 *
	 * @param stmt
	 * @param conn
	 */
	public static void close(Connection conn) {
		close(null, conn, null);
	}

	/**
	 * 关闭连接
	 *
	 * @param stmt
	 * @param conn
	 */
	public static void close(PreparedStatement stmt, Connection conn) {
		close(stmt, conn, null);
	}

	/**
	 * 关闭连接
	 *
	 * @param stmt
	 * @param conn
	 * @param rs
	 */
	public static void close(PreparedStatement stmt, Connection conn, ResultSet rs) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void checkConfig(ConfigBean configBean) {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(configBean.getUrl(), configBean.getUsername(), configBean.getPassword());
			DatabaseMetaData metaData = connection.getMetaData();
			if (!metaData.getTables(null, null, configBean.getTable(), null).next()) {
				throw new YunQueException(CONFIG_ERROR, "数据库不存在表" + configBean.getTable() + "， 请检查配置");
			}
			ResultSet actuals = metaData.getColumns(null, null, configBean.getTable(), null);
			Set<String> expects = Arrays.stream(configBean.getColumn().split(",")).map(String::trim).collect(Collectors.toSet());
			while (actuals.next()) {
				String columnName = actuals.getString("COLUMN_NAME");
				expects.removeIf(expect -> expect.equals(columnName));
			}
			if (!expects.isEmpty()) {
				throw new YunQueException(CONFIG_ERROR, "表 " + configBean.getTable() + " 不存在字段 " + expects);
			}
		} catch (SQLException e) {
			close(connection);
			LOG.error("数据库连接失败检查配置 : " + configBean);
			throw new YunQueException(CONFIG_ERROR ,e);
//			注释该行服务进程报错但是不会退出--返回错误信息到服务端
//			System.exit(PARAMERRORKEXIT.getExitCode());
		}
	}

}
