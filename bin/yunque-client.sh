#!/bin/bash

###
### -h        Show this message.
###
### args:
###   -j  -- job 任务名称
###   -i  -- jobId 任务ID
###   -p  -- path 任务文件路径
###   -f  -- fileFormat 文件类型
###   -s  -- server 服務配置文件
###   -d  -- 容器标识


help() {
    sed -rn 's/^### ?//;T;p;' "$0"
}

if [[ $# == 0 ]] || [[ "$1" == "-h" ]]; then
        help
        exit 1
fi


[ ! -e "$JAVA_HOME/bin/java" ] && JAVA_HOME=$HOME/jdk/java
[ ! -e "$JAVA_HOME/bin/java" ] && JAVA_HOME=/usr/java
[ ! -e "$JAVA_HOME/bin/java" ] && JAVA_HOME=/opt/taobao/java
[ ! -e "$JAVA_HOME/bin/java" ] && unset JAVA_HOME

if [ -z "$JAVA_HOME" ]; then
  if $darwin; then

    if [ -x '/usr/libexec/java_home' ] ; then
      export JAVA_HOME=`/usr/libexec/java_home`

    elif [ -d "/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Home" ]; then
      export JAVA_HOME="/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Home"
    fi
  else
    JAVA_PATH=`dirname $(readlink -f $(which javac))`
    if [ "x$JAVA_PATH" != "x" ]; then
      export JAVA_HOME=`dirname $JAVA_PATH 2>/dev/null`
    fi
  fi
  if [ -z "$JAVA_HOME" ]; then
        error_exit "Please set the JAVA_HOME variable in your environment, We need java(x64)! jdk8 or later is better!"
  fi
fi



export JOB=""
export JOB_ID=""
export FILE_PATH=""
export FILE_FORMAT=""
export SERVER_CONF=""

while getopts ":j:i:f:p:s:d" opt
do		
    case $opt in
        j)
            JOB=$OPTARG;;
        i)
            JOB_ID=$OPTARG;;
        p)
	          FILE_PATH=$OPTARG;;
        f)
            FILE_FORMAT=$OPTARG;;
        s)
            SERVER_CONF=$OPTARG;;
				d)
            DOCKER_FLAG=1;;
        ?)
        echo "Unknown parameter"
        exit 1;;
    esac
done

export JAVA_HOME
export JAVA="$JAVA_HOME/bin/java"
export BASE_DIR=`cd $(dirname $0)/..; pwd`

CONF_DIR=${BASE_DIR}/conf
export CLIENT_MAIN="com.larkmidtable.rpc.YunQueEngineClient"

# 加载环境变量
if [ -f "${CONF_DIR}/yunque-env.sh" ]; then
    . "${CONF_DIR}/yunque-env.sh"
fi

# 读取JVM配置
while read line
do
    if [[ ! $line == \#* ]] && [ -n "$line" ]; then
        JAVA_OPTS="$JAVA_OPTS $line"
    fi
done < ${CONF_DIR}/jvm_client_options

echo ${JAVA_OPTS}

# 启动
${JAVA} ${JAVA_OPT} -cp "${BASE_DIR}/lib/*" ${CLIENT_MAIN} -job ${JOB} -jobId ${JOB_ID} -path ${FILE_PATH} -fileFormat ${FILE_FORMAT} -server ${SERVER_CONF}

